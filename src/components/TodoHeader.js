import React, { useState } from "react";

import { Text, TextInput, View, TouchableOpacity } from "react-native";
import { useDispatch } from "react-redux";
import { addTask } from "../redux/taskSlice";

function TodoHeader() {
  const dispatch = useDispatch();
  const onSubmitTask = () => {
    if (todo.trim().length === 0) {
      alert("Please enter a task");
      setTodo("");
      return;
    }
    dispatch(
      addTask({
        task: todo,
      })
    );
    setTodo("");
  };
  const [todo, setTodo] = useState("");
  return (
    <View>
      <Text
        style={{
          fontSize: 20,
          fontWeight: "bold",
          textAlign: "center",
          marginTop: 50,
        }}
      >
        Todo List
      </Text>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <TextInput
          style={{
            borderColor: "gray",
            borderWidth: 1,
            padding: 10,
            margin: 10,
            width: "80%",
            borderRadius: 5,
          }}
          placeholder="Add Todo"
          onChangeText={(text) => setTodo(text)}
          value={todo}
        />
        <TouchableOpacity
          style={{
            backgroundColor: "black",
            padding: 10,
            margin: 10,
            width: "80%",
            borderRadius: 5,
            alignItems: "center",
          }}
          onPress={onSubmitTask}
        >
          <Text style={{ color: "white" }}> Add</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default TodoHeader;
